#ifndef MY_DIFFBOT_HARDWARE_INTERFACE_H
#define MY_DIFFBOT_HARDWARE_INTERFACE_H

#include <ros/ros.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>
#include <controller_manager/controller_manager.h>

class DiffBotHWInterface : public hardware_interface::RobotHW
{
public:
    DiffBotHWInterface(ros::NodeHandle& nh);
    void init();
    void read(const ros::Duration& period);
    void write(const ros::Duration& period);

private:
    ros::NodeHandle nh_;
    hardware_interface::JointStateInterface jnt_state_interface_;
    hardware_interface::VelocityJointInterface jnt_vel_interface_;
    double cmd_[2];
    double pos_[2];
    double vel_[2];
    double eff_[2];
};

#endif // MY_DIFFBOT_HARDWARE_INTERFACE_H
