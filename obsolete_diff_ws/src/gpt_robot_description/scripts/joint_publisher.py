#!/usr/bin/env python

import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header

def joint_publisher():
    rospy.init_node('joint_publisher')
    pub = rospy.Publisher('/joint_states', JointState, queue_size=10)
    rate = rospy.Rate(10)  # 10hz

    right = 0.0
    left = 0.0
    while not rospy.is_shutdown():
        joint_state = JointState()
        joint_state.header = Header()
        joint_state.header.stamp = rospy.Time.now()
        
        # Example joint names and positions
        joint_state.name = ['base_to_wheel_left', 'base_to_wheel_right']
        joint_state.position = [left, right]

        pub.publish(joint_state)
        right += 1
        left += 1
        rate.sleep()

if __name__ == '__main__':
    try:
        joint_publisher()
    except rospy.ROSInterruptException:
        pass
