# ros_control_tests

ros_control 的简单示例，新手友好

不依赖gazebo，直接在rviz中可视化

示例为两轮差速机器人，但关节控制原理也适用于机械臂

本项目链接：[https://gitcode.com/maizousidemao/ros_control_tests/overview](https://gitcode.com/maizousidemao/ros_control_tests/overview)

有不对的地方或其他疑问，欢迎提 [issues](https://gitcode.com/maizousidemao/ros_control_tests/issues)。



------

## 一、目录说明

1. diff_control：自定义硬件接口
2. use_boilerplate：调用boilerplate使用标准硬件接口
3. obsolete_diff_ws：测试，已废弃





------


## 二、食用方法

每个目录是一个工作空间，进入直接编译，以 `diff_control` 为例：

```bash
# 1. 进入目录
cd diff_control

# 2. 编译，如缺少第三方包或库，自行安装
catkin_make

# 3. 添加环境变量
source devel/setup.bash

# 4. 启动launch脚本
roslaunch my_diffbot diffbot_control.launch

# 其中launch文件中可以选择机器人控制方法
<!-- 启动 teleop_diffbot 节点 -->
<node name="teleop_diffbot" pkg="my_diffbot" type="teleop_diffbot" output="screen"/>

<!-- 调用 rqt_robot_steering 工具控制机器人 -->
<!-- <node name="rqt_robot_steering" pkg="rqt_robot_steering" type="rqt_robot_steering">
	<param name="default_topic" value="/diffbot_velocity_controller/cmd_vel" />
</node> -->
```





------

## 三、交流讨论



![git](img/git.png)
