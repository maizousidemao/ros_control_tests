#include <ros/ros.h>
#include "diff_drive_robot/diff_drive_hardware_interface.h"
#include <controller_manager/controller_manager.h>

int main(int argc, char** argv)
{
  ros::init(argc, argv, "diff_drive_hardware_interface_node");
  ros::NodeHandle nh;

  // 创建机器人专用的硬件接口
  diff_drive_robot::DiffDriveHardwareInterface diff_drive_hw(nh);
  diff_drive_hw.init();

  // 设置控制器管理器
  controller_manager::ControllerManager cm(&diff_drive_hw, nh);

  ros::Rate rate(50); // 50 Hz 控制循环
  ros::AsyncSpinner spinner(1);
  spinner.start();

  while (ros::ok())
  {
    ros::Duration elapsed_time = rate.expectedCycleTime();
    diff_drive_hw.read(elapsed_time);
    cm.update(ros::Time::now(), elapsed_time);
    diff_drive_hw.write(elapsed_time);
    rate.sleep();
  }

  return 0;
}
