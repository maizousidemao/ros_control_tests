#ifndef DIFF_DRIVE_HARDWARE_INTERFACE_H
#define DIFF_DRIVE_HARDWARE_INTERFACE_H

#include <ros_control_boilerplate/generic_hw_interface.h>

namespace diff_drive_robot
{
class DiffDriveHardwareInterface : public ros_control_boilerplate::GenericHWInterface
{
public:
  DiffDriveHardwareInterface(ros::NodeHandle& nh, urdf::Model* urdf_model = nullptr);

  void init() override;
  void read(ros::Duration& elapsed_time) override;
  void write(ros::Duration& elapsed_time) override;

  void enforceLimits(ros::Duration& period) override;  // 实现 enforceLimits 函数
};
}  // namespace diff_drive_robot

#endif  // DIFF_DRIVE_HARDWARE_INTERFACE_H
