#include "my_diffbot/diffbot_hardware_interface.h"

DiffBotHWInterface::DiffBotHWInterface(ros::NodeHandle &nh) : m_nh(nh)
{
}

void DiffBotHWInterface::init()
{
    m_nh.getParam("/hardware_interface/joints", m_joint_names);

    for (size_t i = 0; i < m_joint_names.size(); i++)
    {
        ROS_INFO("get joint: %s", m_joint_names[i].c_str());

        // Initialize hardware interface
        hardware_interface::JointStateHandle state_handle(m_joint_names[i], &m_pos[i], &m_vel[i], &m_eff[i]);
        m_jnt_state_interface.registerHandle(state_handle);

        hardware_interface::JointHandle vel_handle(m_jnt_state_interface.getHandle(m_joint_names[i]), &m_cmd[i]);
        m_jnt_vel_interface.registerHandle(vel_handle);
    }

    registerInterface(&m_jnt_state_interface);
    registerInterface(&m_jnt_vel_interface);
}

void DiffBotHWInterface::read(const ros::Duration &period)
{
    // Read the state of the hardware (e.g., from sensors)
    for (size_t i = 0; i < m_joint_names.size(); i++)
    {
        m_pos[i] += m_vel[i] * period.toSec();
    }
}

void DiffBotHWInterface::write(const ros::Duration &period)
{
    // Send the command to the hardware (e.g., to actuators)
    for (size_t i = 0; i < m_joint_names.size(); i++)
    {
        m_vel[i] = m_cmd[i];
    }
}
