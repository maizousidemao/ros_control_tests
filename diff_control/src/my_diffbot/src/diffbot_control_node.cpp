#include <ros/ros.h>
#include <controller_manager/controller_manager.h>
#include "my_diffbot/diffbot_hardware_interface.h"

int main(int argc, char** argv)
{
    ros::init(argc, argv, "diffbot_control_node");
    ros::NodeHandle nh;

    DiffBotHWInterface diffbot(nh);
    diffbot.init();

    controller_manager::ControllerManager cm(&diffbot, nh);

    ros::Rate rate(50.0);
    ros::AsyncSpinner spinner(1);
    spinner.start();

    while (ros::ok())
    {
        ros::Duration period = rate.expectedCycleTime();
        diffbot.read(period);
        cm.update(ros::Time::now(), period);
        diffbot.write(period);
        rate.sleep();
    }

    return 0;
}
