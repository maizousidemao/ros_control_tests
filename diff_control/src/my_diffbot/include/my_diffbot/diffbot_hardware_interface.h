#ifndef MY_DIFFBOT_HARDWARE_INTERFACE_H
#define MY_DIFFBOT_HARDWARE_INTERFACE_H

#include <ros/ros.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>
#include <controller_manager/controller_manager.h>

class DiffBotHWInterface : public hardware_interface::RobotHW
{
public:
    DiffBotHWInterface(ros::NodeHandle& nh);
    void init();
    void read(const ros::Duration& period);
    void write(const ros::Duration& period);

private:
    ros::NodeHandle m_nh;
    hardware_interface::JointStateInterface m_jnt_state_interface;
    hardware_interface::VelocityJointInterface m_jnt_vel_interface;
    std::vector<std::string> m_joint_names;
    double m_cmd[2];
    double m_pos[2];
    double m_vel[2];
    double m_eff[2];
};

#endif // MY_DIFFBOT_HARDWARE_INTERFACE_H
